﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Event_Manager.Models;
using System.Data;
using System.Data.OleDb;

namespace Event_Manager.Controlers
{
    public class EventControler
    {
        private IDbConnection connection = null;

        public EventControler(string dataFilePath)
        {
            this.connection = new OleDbConnection();
            this.connection.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + dataFilePath;
        }
        private int GetNextId()
        {            
            IDbCommand command = this.connection.CreateCommand();
            command.Connection = this.connection;
            command.CommandText = @"SELECT * FROM Events";

            int id = 1;
            try
            {
                this.connection.Open();
                IDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    EventModel model = new EventModel();
                    model.Id = Convert.ToInt32(reader["Id"]);
                    model.Name = Convert.ToString(reader["Name"]);
                    model.Location = Convert.ToString(reader["Location"]);
                    model.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    model.EndDate = Convert.ToDateTime(reader["StartDate"]);

                    if (id <= model.Id)
                    {
                        id = model.Id + 1;
                    }
                }
            }
            finally
            {
                this.connection.Close();
            }
            return id;

        }
        public EventModel GetById(int id)
        {
            IDbCommand command = this.connection.CreateCommand();
            command.Connection = this.connection;
            command.CommandText = @"SELECT * FROM Events";            

            try
            {
                this.connection.Open();
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    EventModel model = new EventModel();
                    model.Id = Convert.ToInt32(reader["Id"]);
                    model.Name = Convert.ToString(reader["Name"]);
                    model.Location = Convert.ToString(reader["Location"]);
                    model.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    model.EndDate = Convert.ToDateTime(reader["StartDate"]);

                    if (model.Id == id)
                    {
                        return model;
                    }
                }
            }
            finally
            {
                this.connection.Close();
            }
            
            return null;
        }

        public List<EventModel> GetAll()
        {
            List<EventModel> results = new List<EventModel>();

            IDbCommand command = this.connection.CreateCommand();
            command.Connection = this.connection;
            command.CommandText = @"SELECT * FROM Events";            


            try
            {
                this.connection.Open();
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    EventModel model = new EventModel();
                    model.Id = Convert.ToInt32(reader["Id"]);
                    model.Name = Convert.ToString(reader["Name"]);
                    model.Location = Convert.ToString(reader["Location"]);
                    model.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    model.EndDate = Convert.ToDateTime(reader["StartDate"]);

                    results.Add(model);                    
                }
            }
            finally
            {
                this.connection.Close();
            }
            return results;

        }
        private void Insert(EventModel model)
        {
            model.Id = GetNextId();

            IDbCommand command = this.connection.CreateCommand();
            command.CommandText = @"INSERT INTO Events (Id, Name, Location, StartDate, EndDate)
                                    VALUES (@Id, @Name, @Location, @StartDate, @EndDate)";

            IDbDataParameter par = command.CreateParameter();
            par.ParameterName = "@Id";
            par.Value = model.Id ;
            command.Parameters.Add(par);

            par = command.CreateParameter();
            par.ParameterName = "@Name";
            par.Value = model.Name;
            command.Parameters.Add(par);

            par = command.CreateParameter();
            par.ParameterName = "@Location";
            par.Value = model.Location;
            command.Parameters.Add(par);

            par = command.CreateParameter();
            par.ParameterName = "@StartDate";
            par.Value = model.StartDate;
            command.Parameters.Add(par);

            par = command.CreateParameter();
            par.ParameterName = "@EndDate";
            par.Value = model.EndDate;
            command.Parameters.Add(par);

            try
            {
                this.connection.Open();
                command.ExecuteNonQuery();
                Console.WriteLine("Event saved successfully.");
            }
            finally
            {
                this.connection.Close();
            }
        }

        private void Update(EventModel model)
        {
            IDbCommand command = this.connection.CreateCommand();
            command.CommandText = @"UPDATE Events SET Name = @Name, Location = @Location, StartDate = @StartDate, EndDate = @EndDate WHERE Id = @Id";

            IDbDataParameter par = command.CreateParameter();
            par.ParameterName = "@Name";
            par.Value = model.Name;
            command.Parameters.Add(par);

            par = command.CreateParameter();
            par.ParameterName = "@Location";
            par.Value = model.Location;
            command.Parameters.Add(par);

            par = command.CreateParameter();
            par.ParameterName = "@StartDate";
            par.Value = model.StartDate;
            command.Parameters.Add(par);

            par = command.CreateParameter();
            par.ParameterName = "@EndDate";
            par.Value = model.EndDate;
            command.Parameters.Add(par);

            par = command.CreateParameter();
            par.ParameterName = "@Id";
            par.Value = model.Id;
            command.Parameters.Add(par);

            try
            {
                this.connection.Open();
                command.ExecuteNonQuery();
                Console.WriteLine("Event saved successfully.");
            }
            finally
            {
                this.connection.Close();
            }

        }
        public void Delete(EventModel model)
        {
            IDbCommand command = this.connection.CreateCommand();
            command.CommandText = @"DELETE FROM Events WHERE Id = @Id";

            IDbDataParameter par = command.CreateParameter();
            par.ParameterName = "@Id";
            par.Value = model.Id;
            command.Parameters.Add(par);

            try
            {
                this.connection.Open();
                command.ExecuteNonQuery();
                Console.WriteLine("Event deleted successfully.");
            }
            catch
            {
                Console.WriteLine("There was an error deleting event " + model.Name + ".");
            }
            finally
            {
                this.connection.Close();
            }
        }

        public void Save(EventModel model)
        {
            if (model.Id > 0)
            {
                Update(model);
            }
            else
            {
                Insert(model);
            }
        }
    }
}
