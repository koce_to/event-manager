﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Event_Manager.Menu;
using Event_Manager.Controlers;
using Event_Manager.Models;

namespace Event_Manager.Views
{
    public class EventView
    {

        public void Show()
        {
            while (true)
            {
                EventMenu choise = Menu();

                try
                {
                    switch (choise)
                    {
                        case EventMenu.Select:
                            {
                                GetAll();
                                break;
                            }
                        case EventMenu.Insert:
                            {
                                Add();
                                break;
                            }
                        case EventMenu.Update:
                            {
                                Update();
                                break;
                            }
                        case EventMenu.Delete:
                            {
                                Delete();
                                break;
                            }
                        case EventMenu.Exit:
                            {
                                return;
                            }
                    }
                }
                catch (Exception ex)
                {
                    Console.Clear();
                    Console.WriteLine(ex.Message);
                    Console.ReadKey(true);
                }

            }
        }
        
        private void GetAll()
        {
            Console.Clear();
            EventControler eventControler = new EventControler(@"../../Database/data.mdb");

            List<EventModel> models = eventControler.GetAll();

            if (models.Count == 0)
            {
                Console.WriteLine("No Events!");
                Console.ReadKey(true);
                return;
            }

            foreach (EventModel model in models)
            {
                Console.WriteLine("Id: " + model.Id);
                Console.WriteLine("Title: " + model.Name);
                Console.WriteLine("Description: " + model.Location);
                Console.WriteLine("Title: " + model.StartDate);
                Console.WriteLine("Description: " + model.EndDate);
            }
            Console.ReadKey(true);
        }

        private void Add()
        {
            Console.Clear();

            EventModel model = new EventModel();
            Console.WriteLine("Add new Event:");
            Console.Write("Name: ");
            model.Name = Console.ReadLine();
            while (string.IsNullOrEmpty(model.Name))
            {
                Console.Write("Event name is required!");
                model.Name = Console.ReadLine();
            }                        
            Console.Write("Location: ");
            model.Location = Console.ReadLine();
            while (string.IsNullOrEmpty(model.Location))
            {
                Console.Write("Event location is required!");
                model.Location = Console.ReadLine();
            }
            Console.Write("Start date (e.g. dd.mm.yyyy HH:mm): ");
            model.StartDate = Convert.ToDateTime(Console.ReadLine());
            Console.Write("End date: (e.g. dd.mm.yyyy HH:mm)");
            model.EndDate = Convert.ToDateTime(Console.ReadLine());            

            EventControler eventControler = new EventControler(@"../../Database/data.mdb");
            eventControler.Save(model);
            
            Console.ReadKey(true);
        }

        private void Update()
        {
            Console.Clear();
            Console.WriteLine("Enter event ID: ");
            int eventId = Convert.ToInt32(Console.ReadLine());

            EventControler eventControler = new EventControler(@"../../Database/data.mdb");
            EventModel model = eventControler.GetById(eventId);
            if (model == null)
            {
                Console.Clear();
                Console.WriteLine("Event not found.");
                Console.ReadKey(true);
                return;
            }

            Console.WriteLine("Update event ***" + model.Name + "***");
            Console.WriteLine("ID:" + model.Id);

            Console.WriteLine("Name: " + model.Name + "");
            Console.Write("New name:");
            string name = Console.ReadLine();

            Console.WriteLine("Location: " + model.Location + "");
            Console.Write("New location:");
            string location = Console.ReadLine();

            Console.WriteLine("Start date: " + model.StartDate + "");
            Console.Write("New start date:");
            DateTime startDate = Convert.ToDateTime(Console.ReadLine());

            Console.WriteLine("End date: " + model.EndDate + "");
            Console.Write("New end date:");
            DateTime endDate = Convert.ToDateTime(Console.ReadLine());

            if (!string.IsNullOrEmpty(name))
            {
                model.Name = name;
            }
            if (!string.IsNullOrEmpty(location))
            {
                model.Location = location;
            }
            if (DateTime.TryParse("dd:mm:yyyy HH:mm", out startDate))
            {
                model.StartDate = startDate;
            }
            if (DateTime.TryParse("dd:mm:yyyy HH:mm", out endDate))
            {
                model.EndDate = endDate;
            }
            eventControler.Save(model);
            
            Console.ReadKey(true);
        }

        private void Delete()
        {
            EventControler eventControler = new EventControler(@"../../Database/data.mdb");

            Console.Clear();

            Console.WriteLine("Delete event:");
            Console.Write("Event Id: ");

            int eventId = Convert.ToInt32(Console.ReadLine());

            EventModel model = eventControler.GetById(eventId);

            if (model == null)
            {
                Console.WriteLine("Event not found!");
                Console.ReadKey(true);
            }
            else
            {
                eventControler.Delete(model);
            }

            Console.ReadKey(true);
        }

        private EventMenu Menu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Events management:");
                Console.WriteLine("[G]et all Events");
                Console.WriteLine("[A]dd Event");
                Console.WriteLine("[E]dit Event");
                Console.WriteLine("[D]elete Event");
                Console.WriteLine("E[x]it");

                string choice = Console.ReadLine();
                switch (choice.ToUpper())
                {
                    case "G":
                        {
                            return EventMenu.Select;
                        }
                    case "A":
                        {
                            return EventMenu.Insert;
                        }
                    case "E":
                        {
                            return EventMenu.Update;
                        }
                    case "D":
                        {
                            return EventMenu.Delete;
                        }
                    case "X":
                        {
                            return EventMenu.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }
    }
}
